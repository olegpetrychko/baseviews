from django.urls import path, include
from book import views
from rest_framework import routers

router = routers.SimpleRouter()
router.register(r'book', views.BookViewSet)

urlpatterns = [
    path('fbv', views.fbv_book_list, name='fbv-book'),
    path('fbv-drf', views.fbv_api_book_list, name='fbv-drf-book'),
    path('cbv', views.CBVBook.as_view(), name='cbv-book'),
    path('cbv-drf', views.CBVAPIBook.as_view(), name='cbv-drf-book'),
    path('cbv/viewset', views.ReadOnlyBookViewSet.as_view({'get': 'list'}), name='books'),
    path('cbv/viewset/<int:id>', views.ReadOnlyBookViewSet.as_view({'get': 'retrieve'}), name='book'),
    path('', include(router.urls))
]
