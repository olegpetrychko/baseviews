from django.db import models


class Book(models.Model):
    title = models.CharField(max_length=32)
    description = models.TextField()
    pages = models.IntegerField()

    def __str__(self):
        return f'{self.title}'
