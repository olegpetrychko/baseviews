from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet
from django.views import View
from django.shortcuts import render
import json
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from .models import Book
from .serializers import BookSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ViewSet
from rest_framework import permissions


def fbv_book_list(request):
    """
    A standard view function that processes a request and returns a response.
    """
    if request.method == 'GET':
        books = Book.objects.all()
        list_data = []
        for book in books:
            book_dict = {
                'id': book.id,
                'title': book.title,
                'description': book.description,
                'pages': book.pages
            }
            list_data.append(book_dict)
        return render(request, 'book.html', {'title': 'FBV Book', 'list_data': list_data})


@api_view()
def fbv_api_book_list(request):
    """
    Decorated view function that processes the request and returns the response using api.
    api_view () returns the GET method by default. api_view () equals to @api_view(['GET'])
    """
    books = Book.objects.all()
    serializer = BookSerializer(books, many=True)
    return Response({'message': 'From FBV API Book!', 'data': serializer.data})


class CBVBook(View):
    """
    Our view class for the book is inherited from the base-View class.
    Now Django understands how to work with him and how to call him.
    We use .get() method for receiving data.
    """
    template_name = 'book.html'
    books = Book.objects.all()

    def get(self, request, *args, **kwargs):
        list_data = []
        for book in self.books:
            book_dict = {
                'id': book.id,
                'title': book.title,
                'description': book.description,
                'pages': book.pages
            }
            list_data.append(book_dict)
        return render(request, self.template_name, {'title': 'CBV Book', 'list_data': list_data})


class CBVAPIBook(APIView):
    """
    The CBVAPIBook class for the book is inherited from the base-APIView class.
    Now Django understands how to work with him and how to call him in REST architecture.
    We use .get() method for receiving data.
    """
    permission_classes = [permissions.IsAdminUser]

    def get(self, request, *args, **kwargs):
        books = Book.objects.all()
        serializer = BookSerializer(books, many=True)
        return Response({'message': 'From CBV API Book!', 'data': serializer.data})


class ReadOnlyBookViewSet(ReadOnlyModelViewSet):
    """
    The ReadOnlyBookViewSet class inherits from ReadOnlyModelViewSet and includes implementations for various actions,
    by mixing in the behavior of the various mixin classes. The actions provided by the ReadOnlyModelViewSet class are
    .list () and .retrieve ().
    """
    serializer_class = BookSerializer
    queryset = Book.objects.all()

    def list(self, request):
        """
        The function allows you to return data in three ways:
            -default for DRF;
            -using html-template;
            -json-string.
        Appeal to the same address.
        """
        serializer = self.get_serializer(self.get_queryset(), many=True)

        # default for DRF
        return Response(serializer.data)

        # html-template
        # list_data = []
        # for i in serializer.data:
        #     list_data.append(dict(i))
        # return render(request, self.template_name, {'title': 'CBV ViewSet Book', 'list_data': list_data})

        # json-string
        # json_dump = json.dumps(serializer.data)
        # return HttpResponse(json_dump)

    def retrieve(self, request, id=None):
        book = get_object_or_404(self.get_queryset(), id=id)
        serializer = self.get_serializer(book)
        return Response(serializer.data)


class BookViewSet(ModelViewSet):
    """
    The BookViewSet class inherits from ModelViewSet and includes implementations for various actions,
    by mixing in the behavior of the various mixin classes. The actions provided by the ModelViewSet class are
    .list (), .retrieve (), .create (), .update (), .partial_update (), and .destroy ().
    """
    serializer_class = BookSerializer
    queryset = Book.objects.all()


class UserViewSet(ViewSet):
    """
    Example empty viewset demonstrating the standard
    actions that will be handled.
    """

    def list(self, request):
        pass

    def create(self, request):
        pass

    def retrieve(self, request, pk=None):
        pass

    def update(self, request, pk=None):
        pass

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        pass
